<?php

namespace KDA\Backpack\FileManager\Commands;

use Illuminate\Console\Command;
use Backpack\CRUD\app\Console\Commands\Traits\PrettyCommandOutput;
use Illuminate\Support\Str;
use Config;

class Install extends Command
{
   
    use PrettyCommandOutput;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:backpack:filemanager:install
                                    {--timeout=300} : How many seconds to allow each process to run.
                                    {--debug} : Show process output or not. Useful for debugging.';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
       
        $this->progressBar = $this->output->createProgressBar(4);
        $this->progressBar->minSecondsBetweenRedraws(0);
        $this->progressBar->maxSecondsBetweenRedraws(120);
        $this->progressBar->setRedrawFrequency(1);
        $this->progressBar->start();

        $this->line(' Creating uploads directory');
        switch (DIRECTORY_SEPARATOR) {
            case '/': // unix
                $createUploadDirectoryCommand = ['mkdir', '-p', 'public/uploads'];
                break;
            case '\\': // windows
                if (! file_exists('public\uploads')) {
                    $createUploadDirectoryCommand = ['mkdir', 'public\uploads'];
                }
                break;
        }
        if (isset($createUploadDirectoryCommand)) {
            $this->executeProcess($createUploadDirectoryCommand);
        }

        $this->line(' Publishing elFinder assets');
        $this->executeProcess(['php', 'artisan', 'elfinder:publish']);

        $this->line(' Publishing custom elfinder views');
        $this->executeArtisanProcess('vendor:publish', [
            '--provider' => 'Backpack\FileManager\FileManagerServiceProvider',
        ]);

     

        $this->progressBar->finish();
        $this->info(' Backpack\FileManager installed.');

    }
}
