<?php

namespace KDA\Backpack\FileManager;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasDynamicSidebar;

    protected $_commands = [
        Commands\Install::class,
    ];


    protected $sidebars = [

        [
            'route'=>'elfinder',
            'label'=> 'File manager',
            'icon'=>'la-folder-open'
        ]
    ];


    
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}
